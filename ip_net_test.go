package ip_net

import "testing"

// 1.1.128.0/17 1.1.128.0/18 1.1.128.0/19 1.1.128.0/21

var tests = []struct {
	address string
	ip      *IPNet
}{
	{"192.168.1.1/24", nil},
	{"207.123.4.0/24", nil},
	{"168.147.0.0/16", nil},
	{"1.1.128.0/17", nil},
	{"1.1.128.0/18", nil},
	{"1.1.128.0/19", nil},
	{"1.1.128.0/21", nil},
}

func TestConvert(t *testing.T) {

	for x, test := range tests {
		ip, err := New(test.address)
		t.Log("testing ", test.address)
		if err != nil {
			t.Log("error: cound not convert string to address")
			t.Fail()
		}

		tests[x].ip = &ip

		if ip.Addr() != tests[x].address {
			t.Log("error: cound not convert back to string")
			t.Fail()
		}
	}
}

func TestBitstring(t *testing.T) {
	for _, test := range tests {
		original_address, _ := New(test.address)
		converted_address := New_from_bitstring(test.ip.Bstring2(true)).Addr()
		t.Log("")
		t.Log("test = ", test.address)
		t.Log("original_address = ", original_address)
		t.Log("converted_address = ", converted_address)
		if original_address.Network() != converted_address {
			t.Log("error: cound not convert binary ", original_address.Network(), " != ", converted_address)
			t.Fail()
		}
	}
}

func TestBitlength(t *testing.T) {
	for _, test := range tests {
		t.Log("Address = ", test.address)
		t.Log("MaskLength = ", test.ip.MaskLength())
	}
}
