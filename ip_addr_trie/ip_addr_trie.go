package ip_addr_trie

/*
    Implement an address trie.

    This package should probably be re-factored and implement a trie class.  The package
    variables can then be turned into class instance variables so multiple tries can be implemented.

*/

import (
	"fmt"
	//	"log"
	"encoding/json"
	"gitlab.com/sherwood/ip_net"
	"strconv"
	"strings"
)


var level = 0
var path string
var Debuglvl int
var delete_descendants = false
var id int
var parent []int

// The number of symbols in the alphabet - in this case [01]
const num_children = 2

// The trie node
type node struct {
	children    [num_children]*node
	descendants uint //default = 0
	is_node     bool //default = false
	id	int
}

//type childfunctype func (n *node) []string
//var childfunc childfunctype = get_children

// Function to return a root node.
func New() node {
	n := node{}
	parent = append(parent,0)
	return n
}

// delete descendants after get_children() has visited them
func (n *node) Delete_descendants(delete bool) {
	delete_descendants = delete
}

// Add an address to the trie.
// The bitstring is the bit representation of the subnet and is (mask_length) long.
// Adding a bitstring will create one node for each bit, so a /24 subnet will add 24 nodes
func (n *node) Add(bitstring string) {
	if Debuglvl > 1 {
		fmt.Println("=== In Add(", bitstring, ") ===")
	}
	n.descendants += 1
	if len(bitstring) > 0 {
		char, _ := strconv.Atoi(string(bitstring[0]))
		rest := bitstring[1:]
		if Debuglvl > 1 {
			fmt.Println("  char = ", char)
			fmt.Println("  rest = ", rest)
		}
		if n.children[char] == nil {
			new_node := new(node)
			n.children[char] = new_node
		}
		n.children[char].Add(rest)
	} else {
		n.is_node = true
		n.id = id
		id += 1
	}
}


func print_node(n *node, parent int) {
	address := ip_net.New_from_bitstring(path)
	fmt.Printf("{\n")
	fmt.Printf("  \"id\": %d,\n",n.id)
	fmt.Printf("  \"name\": \"%s\",\n",address)
	fmt.Printf("  \"parent\": %d,\n",parent)
	fmt.Printf("},\n")
}



// Traverse the trie in depth first order.
func (n *node) Dfs() {
	//fmt.Println("=== In dfs() ===")
	//fmt.Println(strings.Repeat(" ", level*2), "path =", path, "level =", level, "descendants =", n.descendants, "is_node =", n.is_node)
	if (n.is_node) {
		print_node(n,parent[len(parent)-1])
		parent = append(parent,n.id)
	}
	level += 1
	for x := 0; x < num_children; x++ {
		if n.children[x] != nil {
			path += strconv.Itoa(x)
			//fmt.Println(strings.Repeat(" ", level*2), "x= ", x)
			n.children[x].Dfs()
			path = path[:len(path)-1]
		}
	}
	level -= 1
	if (n.is_node) {
		parent = parent[:len(parent)-1]
	}
}

// Traverse the trie in depth first order.  If delete_descendants is set,
// we return a slice of the descendants and delete them.
func (n *node) Dfs2() {
	//fmt.Println("=== In dfs2() ===")
	//fmt.Println(strings.Repeat(" ", level*2), "path =", path, "level =", level, "descendants =", n.descendants, "is_node =", n.is_node)

	var cl []string

	// If this is a subnet (is_node==true), find all children
	if n.is_node == true {
		//fmt.Println(strings.Repeat(" ", level*2), "path =", path, "level =", level, "descendants =", n.descendants, "is_node =", n.is_node)

		cl = n.get_children()

		// print out data - the subnet and dependents
		address := ip_net.New_from_bitstring(path)
		fmt.Printf("%s,", address.Addr())
		fmt.Printf("%d,", n.descendants-1)
		if n.descendants > 1 {
			j, _ := json.Marshal(cl)
			fmt.Printf("%s", j)
			//fmt.Printf("%v", cl)
		}
		fmt.Printf("\n")

	}

	// trace all descendants of the current node
	level += 1 // level increases since we are looking at children of current node
	for x := 0; x < num_children; x++ {
		if n.children[x] != nil {
			path += strconv.Itoa(x)
			n.children[x].Dfs2()
			path = path[:len(path)-1]
		}
	}
	level -= 1
}

// do a DFS traversal and make a slice of all descendents along the way.
func (n *node) get_children() []string {
	var cl []string // child list

	level += 1
	for x := 0; x < num_children; x++ {
		if n.children[x] != nil {
			path += strconv.Itoa(x)
			if n.children[x].is_node == true {
				address := ip_net.New_from_bitstring(path)
				cl = append(cl, address.Addr())
			}
			cl = append(cl, n.children[x].get_children()...)
			path = path[:len(path)-1]

			if delete_descendants {
				n.children[x] = nil
			}
		}

	}
	level -= 1
	return cl
}

// WIP - Summ up the utilized address space for each node in the trie
func (n *node) Summ(percent [32]int) {
	fmt.Println("=== In Summ() ===")
	fmt.Println("percent = ", percent)
	// Caculate percent and compare to descendants // fmt.Println()
	fmt.Println(strings.Repeat(" ", level*2), "path =", path, "level =", level, "descendants =", n.descendants, "is_node =", n.is_node)
	level += 1
	for x := 0; x < num_children; x++ {
		if n.children[x] != nil {
			path += strconv.Itoa(x)
			//fmt.Println(strings.Repeat(" ", level*2), "x= ", x)
			n.children[x].Summ(percent)
			path = path[:len(path)-1]
		}
	}
	level -= 1
}
