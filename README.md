# ip_net
[![Go Reference](https://pkg.go.dev/badge/gitlab.com/sherwood/ip_net.svg)](https://pkg.go.dev/gitlab.com/sherwood/ip_net)

Package to work with IPv4 addresses in various formats.

A goal of this package was for me to learn Go.  Since this is my first go package, i'm sure it needs some work ;)


## TODO
- rewrite the methods/functions to make them more symmetric/go'ish.
- performance test various implementations - the current ones are just what worked first.
- have New accept multiple formats.
- add IPv6.
- look at Vega (https://vega.github.io/vega-lite/) to graph address space.

## ip_addr_trie
ip_addr_trie is a [Trie](https://en.wikipedia.org/wiki/Trie)  package to work with network addresses.  In this case it is an [IP address prefix trie](https://www.lewuathe.com/longest-prefix-match-with-trie-tree.html)

### func Delete_descendants(delete bool)
  Delete descendants once a node with descendants is first visited.
  This will summarize all descentants under the largest prefix that covers them.

Ex:

Without delete descendants, each prefix may be visited multiple times:
```
1.1.128.0/17,3,["1.1.128.0/18","1.1.128.0/19","1.1.128.0/21"]
1.1.128.0/18,2,["1.1.128.0/19","1.1.128.0/21"]
1.1.128.0/19,1,["1.1.128.0/21"]
1.1.128.0/21,0,
```

With delete descendants, only the 1.1.128.0/17 prefix will be visited.  All children of 1.1.128.0/17 have been deleted:
```
1.1.128.0/17,3,["1.1.128.0/18","1.1.128.0/19","1.1.128.0/21"]
```

