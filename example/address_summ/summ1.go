package main

// https://www.socketloop.com/tutorials/golang-convert-ipv4-address-to-packed-32-bit-binary-format

import (
	"flag"
	"fmt"
	"gitlab.com/sherwood/ip_net/ip_addr_trie"
	//"log"
	//"net"
	//"strconv"
	//"strings"
	//    "encoding/binary"
	//    "math/big"
	//    "bytes"
)

var Debuglvl int

func main() {

	// flags declaration using flag package
	flag.IntVar(&Debuglvl, "debug", 0, "Debug level")
	flag.Parse()

	root := ip_addr_trie.New()
	//root.Add("0")
	//root.Add("1")
	//root.Add("101011")
	//root.Add("101000")
	//root.Add("1010")

	//networks := []string{"10.0.0.0/8", "192.168.0.1/32", "192.168.1.1/16","2001:db8::2:1/64"}
	networks := []string{"10.0.0.0/8", "192.168.0.1/32", "192.168.1.1/16", "1.1.1.1/26", "71.58.36.200/26", "71.58.36.199/26", "71.58.36.198/26", "71.58.36.197/26"}

	//ip1, ipv4Net, err := net.ParseCIDR("10.0.0.0/8")
	//ip1, ipv4Net, err := net.ParseCIDR("192.168.0.1/32")
	for i := 0; i < len(networks); i++ {
		fmt.Println("\nstring =", networks[i])
		bstring := ip_addr_trie.Addrtobstr(networks[i])
		root.Add(bstring)
		fmt.Println("Addrtobstr = ", bstring)
		fmt.Println("Bstrtoaddr = ", ip_addr_trie.Bstrtoaddr(bstring))
	}

	//root.Dfs()
	//root.Summ([{24: 10, 26: 2}])
	percent := [32]int{24: 10, 26: 20}
	fmt.Println("percent = ", percent)
	root.Summ(percent)

}
