module gitlab.com/sherwood/ip_net/address_breakdown

go 1.17

replace gitlab.com/sherwood/ip_net => ../../

require gitlab.com/sherwood/ip_net v0.0.0-20220408142609-4fda220c64d0
