package main

// go mod edit -replace gitlab.com/sherwood/ip_net=../

import (
	"bufio"
	"flag"
	"fmt"
	"gitlab.com/sherwood/ip_net"
	"gitlab.com/sherwood/ip_net/ip_addr_trie"
	"os"
)

func main() {

	delete := flag.Bool("delete", false, "delete descendants if they are covered by a larger subnet")
	flag.Parse()

	if len(flag.Args()) != 1 {
		fmt.Println("Need to specify file of prefixes")
		flag.PrintDefaults()
		os.Exit(1)
	}

	readFile, err := os.Open(flag.Arg(0))
	if err != nil {
		fmt.Println(err)
	}

	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	trie := ip_addr_trie.New()
	trie.Delete_descendants(*delete)

	for fileScanner.Scan() {
		ip, err := ip_net.New(fileScanner.Text())
		if err == nil {
			trie.Add(ip.Bstring2(true))
		}
	}

	readFile.Close()

	//trie.Dfs2()
	trie.Dfs()

	//percent := [32]int{24:10, 26:20}
	//fmt.Println ("percent = ",percent)
	//trie.Summ(percent)

}
