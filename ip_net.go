/*
ip_net provides various format conversions for ip address manipulation.
*/

package ip_net

// Package to convert network addresses to/from various formats

// This looks like this is a more complete package:
// https://github.com/c-robinson/iplib
// I did not find this till after I created this library.
//
// Stanford bit hacks:
// https://graphics.stanford.edu/~seander/bithacks.html
//
//
// https://medium.com/learning-the-go-programming-language/bit-hacking-with-go-e0acee258827

// _to_ => output, change value _to_ something
// _from_ => input, take value _from_ something

import (
	"errors"
	"fmt"
	"log"
	"math/bits"
	"strconv"
	"strings"
)

// The IPNet structure, just a slice of bytes for the IP address and the netmask.
type IPNet struct {
	IP   []byte
	Mask []byte
}

// Create and return a new IPNet opject.
// Currently parses a string value of an address.
// TODO: Acceps Uint32, bitstring, and hex
func New(subnet string) (IPNet, error) {
	// Split address a.b.c.d/f on "/" to separate address and bitlength fields
	s := strings.Split(subnet, "/") // replace with strings.Cut in golang 1.18
	if len(s) != 2 {
		log.Fatal("bad IP address format - missing '/'")
		return IPNet{}, errors.New("bad address format - missing '/'")
	}

	// split address
	a := strings.Split(s[0], ".")
	if len(a) != 4 {
		log.Fatal("bad IP address format - need 3 dots")
		return IPNet{}, errors.New("bad IP address format - '.'")
	}

	// get mask bit length
	mask_bitlength, _ := strconv.Atoi(s[1])

	// convert byte strings to bytes
	i, _ := strconv.Atoi(a[0])
	j, _ := strconv.Atoi(a[1])
	k, _ := strconv.Atoi(a[2])
	l, _ := strconv.Atoi(a[3])

	// Assemble address byte array
	addr_byte_array := []byte{
		byte(i),
		byte(j),
		byte(k),
		byte(l),
	}

	// caculate mask uint32 from bitlength
	mask_uint32 := uint32((0xffffffff << (32 - mask_bitlength)))

	// Assemble mask byte array
	mask_byte_array := []byte{
		byte(mask_uint32 >> 24),
		byte(mask_uint32 >> 16),
		byte(mask_uint32 >> 8),
		byte(mask_uint32),
	}

	return IPNet{IP: addr_byte_array, Mask: mask_byte_array}, nil
}

// Return the ip address as a Uint32
func (ip IPNet) Uint32() uint32 {
	return (uint32(ip.IP[0]) << 24) + (uint32(ip.IP[1]) << 16) + (uint32(ip.IP[2]) << 8) + (uint32(ip.IP[3]))
}

// Return the IP and netmask as a dotted quad with a "/" and a mask length.
func (ip IPNet) String() string {
	//return fmt.Sprintf("%d.%d.%d.%d", ip.IP[0], ip.IP[1], ip.IP[2], ip.IP[3])
	return fmt.Sprintf("%d.%d.%d.%d/%d", ip.IP[0], ip.IP[1], ip.IP[2], ip.IP[3], ip.MaskLength())
}

// Return the IP address as a quad dotted string
func (ip IPNet) Addr() string {
	//return fmt.Sprintf("%d.%d.%d.%d/%d", ip.IP[0], ip.IP[1], ip.IP[2], ip.IP[3], ip.MaskLength())
	return fmt.Sprintf("%d.%d.%d.%d", ip.IP[0], ip.IP[1], ip.IP[2], ip.IP[3])
}

// Return the network - no host bits
// Ex: if address/mask = 192.168.1.1/16 we return 192.168.0.0/16
func (ip IPNet) Network() string {
	var myip []byte = make([]byte, 4)

	for x := 0; x < 4; x++ {
		myip[x] = ip.IP[x] & ip.Mask[x]
	}
	return fmt.Sprintf("%d.%d.%d.%d/%d", myip[0], myip[1], myip[2], myip[3], ip.MaskLength())
}

// Return the address as a bit string
func (ip IPNet) bstring() string {
	return fmt.Sprintf("%032b", ip.Uint32())
}

// Return the IP address as a binary bit string.
func (ip IPNet) Bstring2(trunc_to_subnet bool) string {
	//format := fmt.Sprintf("%%0%db",ip.Mask_bitlength)
	//fmt.Printf("format: %s\n", format)
	if trunc_to_subnet {
		//return fmt.Sprintf("%032b", ip.Uint32())[0:ip.Mask_bitlength]
		return fmt.Sprintf("%032b", ip.Uint32())[0:ip.MaskLength()]
	} else {
		return fmt.Sprintf("%032b", ip.Uint32())
	}
}

// Convert a bitstring back to an address.
// The lenght of the string represents the subnet mask length.
// If the string is <32 we asssume it represents the most significant bits of a subnet and the misssing zeros are the host.
// Ex: 00001000 = 8/8   11110000 = 240/8   1111000000000000 = 240/16
func New_from_bitstring(bitstring string) IPNet {
	var myip []byte = make([]byte, 4)
	var mymask []byte = make([]byte, 4)
	var byte_position byte

	bitlength := len(bitstring)

	// Pad out with zeros if len < 32
	bitstring = bitstring + strings.Repeat("0", 32-len(bitstring))

	_, err := fmt.Sscanf(bitstring, "%08b%08b%08b%08b", &myip[0], &myip[1], &myip[2], &myip[3])
	if err != nil {
		panic(err)
	}

	for x := 0; x < bitlength; x++ {
		if (x > 0) && (x%8 == 0) {
			byte_position++
		}
		mymask[byte_position] = (mymask[byte_position] << 1) | 1
	}

	return IPNet{IP: myip, Mask: mymask}
}

// Return the length of the subnet mask
func (ip IPNet) MaskLength() int {
	var length = 0
	for x := 0; x < len(ip.Mask); x++ {
		length += bits.OnesCount8(ip.Mask[x])
	}
	return length
}
